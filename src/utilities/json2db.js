/*
------------
jsonToDb.js
------------
Imports logs in JSON format *.json from $DIR_DATA/json to a sqlite database ($DIR_DATA/database.db)
*/
"use strict";
import { config } from "dotenv";
import findConfig from "find-config";
import fs from "fs";
import { exit } from "process";
import vrcLogImport from "../modules/vrcLogImport.js";
import { forceDbRebuild } from "../modules/util.js";
import path from "path";
import knexConfig from "../modules/knex/knexfile.js";
import knex$0 from "knex";
({ config }.config({ path: findConfig(".env") }));
const { importRecords } = vrcLogImport;
const knex = knex$0(knexConfig);
if (envBool(process.env.DB_FORCE_REBUILD)) forceDbRebuild();

const src = path.join(process.env.DIR_DATA, "json");
knex.migrate.latest().then(() => {
  fs.promises
    .readdir(src)
    .then((files) => {
      const jsonFiles = files.filter((file) => file.includes(".json"));
      console.log(
        `Processing ${jsonFiles.length} files${
          jsonFiles.length > 25 ? " (this could take a while)" : ""
        }...`
      );
      const promises = [];
      jsonFiles.forEach(async (file) => {
        let jsonData = JSON.parse(fs.readFileSync(`${src}/${file}`));
        const id = file.replace(".json", "");
        promises.push(importRecords({ knex, id, jsonData }));
      });
      Promise.all(promises)
        .then(() => {
          console.log("Done");
          exit();
        })
        .catch((e) => console.error(e));
    })
    .catch((e) => console.error(e));
});
