export const ANON = "webanon";
export const USER = "webuser";
export const SUPER = "websuper";
export default {
  ANON,
  USER,
  SUPER
};
