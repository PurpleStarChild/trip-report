import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { up as tripReportUp } from "./services/tripReportApi";
import { setServiceStatus } from "./redux/slice/status";
import Error from "./components/Error";
import { RiCloudOffLine } from "react-icons/ri";
import Home from "./pages/Home";

import { BrowserRouter, Route, Routes } from "react-router-dom";

function App() {
  const dispatch = useDispatch();

  // Service Check: Every 15 seconds, verify all systems go
  useEffect(() => {
    const doUptimeCheck = async () => {
      const tripReport = await tripReportUp();
      dispatch(setServiceStatus({ tripReport }));
    };
    doUptimeCheck();
    const timer = setInterval(doUptimeCheck, 15000);
    return () => clearTimeout(timer);
  }, [dispatch]);

  const tripReportService = useSelector(
    (state) => state.status.service.tripReport
  );
  return (
    <div>
      {!tripReportService.up && (
        <Error
          message={"API Unreachable"}
          subMessage={"Is the server running?"}
          icon={<RiCloudOffLine />}
        />
      )}
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="instance/:id" element={<Home />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
