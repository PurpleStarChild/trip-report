"use strict";
import dotenv from "dotenv";
import findConfig from "find-config";
import path from "path";
import express from "express";
import cors from "cors";
import bodyParser from "body-parser";
import Knex from "knex";
import base from "./routes/base.js";
import api from "./routes/api.js";
import knexConfig from "../modules/knex/knexfile.js";
import debug from "./modules/debugLog.js";
import env from "../modules/environment.js";
import { envBool, isProcessRunning } from "../modules/util.js";
import { initializeWatcher } from "./modules/watcher.js";
import { processLogfiles } from "../modules/vrcLogParse.js";
import { fileURLToPath } from "url";

const __dirname = path.dirname(fileURLToPath(import.meta.url));

dotenv.config({ path: findConfig(".env") });
if (!env.validate()) process.exit();
env.display();
const app = express();

const server = {
  app,
  start: (cb, overrideKnex) => {
    app.use(cors());

    // Frontend
    app.use("/", express.static(path.join(__dirname, "public")));
    app.use("/instance/:id", express.static(path.join(__dirname, "public")));

    // Assets
    app.use("/assets", express.static(path.join(__dirname, "DATA/assets")));

    // DB setup
    const knex = overrideKnex ? overrideKnex : Knex(knexConfig);
    knex.migrate.latest().then(async () => {
      // If VRChat isn't running, process any existing logfiles
      const isRunning = await isProcessRunning({
        windows: process.env.VRCHAT_PROCESS_NAME
      });
      if (!isRunning) processLogfiles(knex);

      // Watch for new log files and process them when we can
      if (envBool(process.env.WATCHER_ENABLED)) initializeWatcher();

      // API
      app.use(bodyParser.json());
      app.use((req, res, next) => {
        req.knex = knex;
        next();
      });

      app.use("/api", base);
      app.use("/api", api);

      app.use((req, res) => {
        debug.log(`REQ: 404 - No Route`);
        return res.sendStatus(404);
      });

      if (typeof cb === "function") cb(knex);
    });
  }
};
export default server;
