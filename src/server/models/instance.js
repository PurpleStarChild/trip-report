import STATUS from "../modules/constants/httpCodes.js";
export const read = async (knex, data, cb) => {
  let logEntries = await knex
    .select("*")
    .from("log")
    .where("instance", "=", data.id)
    .andWhereRaw("tag IS NOT NULL")
    .orderBy("ts");
  logEntries = logEntries.map((item) => {
    return { ...item, data: JSON.parse(item.data) };
  });
  cb(STATUS.OK, JSON.stringify(logEntries));
};
export default {
  read
};
