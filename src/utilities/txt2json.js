/*
------------
txtToJson.js
------------
Converts VRC log files in TXT format to JSON format, optionally annotating the log entries

node src/utilities/txtToJson.js --ignore DATA --max-old-space-size=8000
*/
"use strict";
import { config } from "dotenv";
import findConfig from "find-config";
import fs from "fs";
import path from "path";
import { makeDir } from "../modules/util.js";
import { convertToJson } from "../modules/vrcLogParse.js";
import * as JSONStream from "JSONStream";
import { envBool } from "../modules/util.js";
({ config }.config({ path: findConfig(".env") }));
const forceRebuild = envBool(process.env.DB_FORCE_REBUILD);
const skipExisting = !forceRebuild;

fs.promises.readdir(process.env.DIR_VRC_LOG_FILES).then(async (files) => {
  for (const file of files) {
    const logFile = path.join(process.env.DIR_VRC_LOG_FILES, file);
    const dstFolder = path.join(process.env.DIR_DATA, "json");
    makeDir(dstFolder);
    const outputFilename = path.join(dstFolder, file).replace(".txt", ".json");
    let outputExists = false;
    try {
      if (fs.existsSync(outputFilename)) outputExists = true;
    } catch {}
    const input = await fs.promises.stat(logFile);
    if (skipExisting && outputExists) {
      console.log(`Skipping: ${file}`);
    } else {
      if (input.isFile() && file.charAt(0) !== "." && file.includes(".txt")) {
        const jsonData = await convertToJson(logFile);
        const transformStream = JSONStream.stringify();
        const outputStream = fs.createWriteStream(outputFilename);
        transformStream.pipe(outputStream);
        jsonData.forEach(transformStream.write);
        transformStream.end();
      } else {
        console.log(`Ignoring: ${file}`);
      }
    }
  }
});
