/*
---------------------------
bulkingestScreenshots.js
---------------------------
Configured via .env
Bulk-imports screenshots and makes thumbnails
*/

import { config } from "dotenv";
import findConfig from "find-config";
import fs from "fs";
import { exit } from "process";
import {
  ingestScreenshots,
  buildDirectoryCache
} from "../modules/vrcScreenshots.js";
import path from "path";
import { convertToJson } from "../modules/vrcLogParse.js";
import humanizeDuration from "humanize-duration";

("use strict");
({ config }.config({ path: findConfig(".env") }));
let directoryCache = buildDirectoryCache();
let counter = 0;
const start = Date.now();
let imageCount = 0;
fs.promises.readdir(process.env.DIR_VRC_LOG_FILES).then(async (files) => {
  const logFiles = files.filter((file) => file.includes(".txt"));
  logFiles.forEach(async (file) => {
    const logFile = path.join(process.env.DIR_VRC_LOG_FILES, file);
    const input = await fs.promises.stat(logFile);
    if (input.isFile() && file.charAt(0) !== "." && file.includes(".txt")) {
      const jsonData = await convertToJson(logFile);
      const screenshotList = jsonData.filter(
        (item) => item.tag === "screenshot"
      );
      if (screenshotList.length > 0)
        ingestScreenshots(screenshotList, directoryCache);
      counter++;
      imageCount += screenshotList.length;
      if (counter === logFiles.length) {
        const end = Date.now();
        console.log("-----------------------------");
        console.log(
          `Processed ${imageCount.toLocaleString(
            "en-US"
          )} images in ${humanizeDuration(end - start)}`
        );
        exit(); //FIXME: Shouldn't need to do this
      }
    } else {
      console.log(`Ignoring: ${file}`);
      counter++;
    }
  });
});
