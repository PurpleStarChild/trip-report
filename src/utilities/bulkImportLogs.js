import { config } from "dotenv";
import findConfig from "find-config";
import fs from "fs";
import { exit } from "process";
import vrcLogImport from "../modules/vrcLogImport.js";
import path from "path";
import knexConfig from "../modules/knex/knexfile.js";
import knex$0 from "knex";
import { convertToJson } from "../modules/vrcLogParse.js";
import { forceDbRebuild, envBool } from "../modules/util.js";
import humanizeDuration from "humanize-duration";
/*
---------------
bulkImport.js
---------------
Configured via .env

Bulk-imports a folder of logs in TXT format directly into a database.

node src/utilities/bulkImport.js --ignore DATA --max-old-space-size=8000
*/
("use strict");
({ config }.config({ path: findConfig(".env") }));
const { importRecords } = vrcLogImport;
const knex = knex$0(knexConfig);
if (envBool(process.env.DB_FORCE_REBUILD)) forceDbRebuild();
knex.migrate.latest().then(() => {
  let counter = 0;
  const start = Date.now();
  let recordCount = 0;
  fs.promises.readdir(process.env.DIR_VRC_LOG_FILES).then(async (files) => {
    const logFiles = files.filter((file) => file.includes(".txt"));
    if (!logFiles.length) {
      console.log("Nothing to do!");
      process.exit();
    }
    logFiles.forEach(async (file) => {
      const logFile = path.join(process.env.DIR_VRC_LOG_FILES, file);
      const input = await fs.promises.stat(logFile);
      if (input.isFile() && file.charAt(0) !== "." && file.includes(".txt")) {
        const jsonData = await convertToJson(logFile);
        const id = file.replace(".json", "");
        recordCount += jsonData.length;
        await importRecords({
          knex,
          id,
          jsonData
        });
        counter++;
        if (counter === logFiles.length) {
          const end = Date.now();
          console.log("Done!");
          console.log(
            `${recordCount.toLocaleString(
              "en-US"
            )} records in ${humanizeDuration(end - start)})`
          );
          exit(); //FIXME: Shouldn't need to do this
        }
      } else {
        console.log(`Ignoring: ${file}`);
        counter++;
      }
    });
  });
});
