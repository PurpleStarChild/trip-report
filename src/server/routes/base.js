import * as express from "express";
const router = express.Router();
router.get("/up", (req, res) => {
  res.sendStatus(200);
});
export default router;
